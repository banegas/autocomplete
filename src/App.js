import React from 'react';
import Autocomplete from '../src/component/Autocomplete';

function App() {
  return (
    <div className="App container">
     <div className="row" style={{paddingTop:'20px'}}>
       <div className="col-4">
       <Autocomplete backgroundColor={'red'} border={'3px solid #555'} color={'white'} margin={'15px 5px'}/>
       </div>
       <div className="col-4">
        <Autocomplete />
      </div>
      <div className="col-4">
      <Autocomplete width={'50%'} color={'black'} backgroundColor={'lightblue'} margin={'8px 0'}/>
      </div>
     </div>
    </div>
  );
}

export default App;
