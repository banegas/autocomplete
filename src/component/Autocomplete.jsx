import React,{Component} from 'react';
import axios from 'axios';


class Autocomplete extends Component{
    static defaultProps = {
        
            width: "", //'100%',
            padding: "", //12px 20px
            margin: "", //8px 0
            backgroundColor: "",//#3CBC8D
            color: "", //white
            boxSizing: "", //border-box
            border: "" //3px solid #555
        
    }
   
    constructor(props){
        super(props)
        this.state={
            cursor: -1,
            options:[],
            filoptions:[],
            value:"",
            width: this.props.width,
            padding: this.props.padding,
            margin: this.props.margin,
            backgroundColor: this.props.backgroundColor,
            color: this.props.color,
            boxSizing: this.props.boxSizing,
            border: this.props.border
        }
    }
    componentDidMount(){
        this.getColleges();
        window.addEventListener("keyup", this.keyHandling);
    }
    componentWillUnmount(){
        window.removeEventListener("keyup", this.keyHandling);	  
    }

    getColleges= async ()=>{
    axios.get('http://niche-recruiting-autocomplete.appspot.com/search/?query=pitt')
    .then((response)=> {
    // handle success
    const data = this.toJSON(response);
    this.setState({options:data.results});

    })
    .catch((error)=> {
        // handle error
        console.log(error);
        return []
    })
    }
    toJSON=(response)=>{
        let strToarr = response.data.split('');
        strToarr.shift()
        strToarr.pop()
        strToarr.pop()
        strToarr.pop()
        let strTojso = strToarr.join('');
        return JSON.parse(strTojso);
    }
   
    handleOnChange(event){
        let value = event.target.value;
        let  filoptions =[];
        if(value.length>0){
            filoptions = this.state.options.filter(school =>{
                let name = school.name;
                return name.toLowerCase().startsWith(value.toLowerCase());
            });
        }else{

        }
    
        this.setState({value:value});
        this.setState({filoptions:filoptions});
    }
    handleOnClick(index){
       let selected = this.state.filoptions[index];
       window.location = selected.url; 
    }
    keyHandling=(event)=>{
        const {cursor,filoptions} = this.state;
        // arrow up/down button should select next/previous list element

        if (event.keyCode === 38 && cursor > 0) {
          this.setState( prevState => ({
            cursor: prevState.cursor - 1
          }))
        } else if (event.keyCode === 40 && cursor < filoptions.length - 1) {
          this.setState( prevState => ({
            cursor: prevState.cursor + 1
          }))
        }else if(event.keyCode === 13){
            // put the login here
            let selected = filoptions[this.state.cursor];
            window.location = selected.url; 
         }else if(event.keyCode === 8){
            if(this.state.value.length === 0){
                this.setState( prevState => ({
                    cursor: -1
                  }))
            }
         }
      }

    //   width: '100%',
    //   padding: '', //12px 20px
    //   margin: '', //8px 0
    //   backgroundColor: "",//#3CBC8D
    //   color: "", //white
    //   boxSizing: '', //border-box
    //   border: '' //3px solid #555

    render(){
       const {border,width,backgroundColor,color,padding,margin} = this.state;
        return (
            <div className="autocomplete container">
                <div className="row">
                    <div className="col-12">
                    <input type="text" style ={{border:border,width:width,backgroundColor:backgroundColor,color:color,padding:padding,margin:margin}} className="form-control" id="autocomplete-input"  placeholder="Enter school" value={this.state.value} onChange={ this.handleOnChange.bind(this) } />
                        <ul className="list-group" style ={{width:width,backgroundColor:backgroundColor,color:color}} >
                            {
                                this.state.filoptions.map((option,i)=>{
                                    return <li key={i} className={`list-group-item ${this.state.cursor===i?'active':null}`} onClick={this.handleOnClick.bind(this,i)}>{option.name}</li>
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
          );
    }
   
 
}

export default Autocomplete;